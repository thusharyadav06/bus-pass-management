/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bus.pass.management.model.ui.data;

/**
 *
 * @author student
 */
public class PassengerDetails {
    private Integer Id;
    private String Name;
    private String MobNo;
    private String Email;

    public PassengerDetails(Integer Id, String Name, String MobNo, String Email) {
        this.Id = Id;
        this.Name = Name;
        this.MobNo = MobNo;
        this.Email = Email;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getMobNo() {
        return MobNo;
    }

    public void setMobNo(String MobNo) {
        this.MobNo = MobNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }
    
}
