/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bus.pass.management.service.impl;

import in.ac.gpckasaragod.bus.pass.management.model.ui.data.PassengerDetails;
import in.ac.gpckasaragod.bus.pass.management.service.PassengerDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class PassengerDetailsServiceImpl extends ConnectionServiceImpl implements PassengerDetailsService {
     @Override
    public String savePassengerDetails(String name, String mobNo, String email) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO PASSENGER_DETAILS (NAME,MOB_NO,EMAIL) VALUES " 
                    + "('"+name+ "','" +mobNo+ "','" +email+"')" ;
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(PassengerDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
    }
    @Override
    public PassengerDetails readPassengerDetails(Integer Id){
        PassengerDetails passengerDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM PASSENGER_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                int id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String mobNo = resultSet.getString("MOB_NO");
                String email = resultSet.getString("EMAIL");
                 passengerDetails = new PassengerDetails(id,name,mobNo,email);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(PassengerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return passengerDetails;
       
    }
    @Override
    public List<PassengerDetails> getAllPassengerDetails(){
        List<PassengerDetails> passengers = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM PASSENGER_DETAILS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                String mobNo = resultSet.getString("MOB_NO");
                String email = resultSet.getString("EMAIL");
                PassengerDetails passengerDetails = new PassengerDetails(id,name,mobNo,email);
                passengers.add(passengerDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(PassengerDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return passengers;
    
    }
    
    @Override
    public String updatePassengerDetails(Integer id,String name,String mobNo,String email){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE PASSENGER_DETAILS SET NAME='"+name+"',MOB_NO='"+mobNo+"',EMAIL='"+email+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }

    @Override
    public String deletepassengerDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM PASSENGER_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }
    
    
}
