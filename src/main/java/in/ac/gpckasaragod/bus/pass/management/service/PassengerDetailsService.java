/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.bus.pass.management.service;

import in.ac.gpckasaragod.bus.pass.management.model.ui.data.PassengerDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface PassengerDetailsService {
    public String savePassengerDetails(String name, String mobNo, String email);
    public PassengerDetails readPassengerDetails(Integer Id);
    public List<PassengerDetails> getAllPassengerDetails();
    public String updatePassengerDetails(Integer id,String name,String mobNo,String email);
    public String deletepassengerDetails(Integer id);
}
