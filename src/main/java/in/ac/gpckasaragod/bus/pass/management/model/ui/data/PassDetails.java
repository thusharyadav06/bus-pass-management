/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.bus.pass.management.model.ui.data;

/**
 *
 * @author student
 */
public class PassDetails {
    private Integer Id;
    private Integer PassengerId;
    private String TravelFrom;
    private String TravelTo;

    public PassDetails(Integer Id, Integer PassengerId, String TravelFrom, String TravelTo) {
        this.Id = Id;
        this.PassengerId = PassengerId;
        this.TravelFrom = TravelFrom;
        this.TravelTo = TravelTo;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Integer getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(Integer PassengerId) {
        this.PassengerId = PassengerId;
    }

    public String getTravelFrom() {
        return TravelFrom;
    }

    public void setTravelFrom(String TravelFrom) {
        this.TravelFrom = TravelFrom;
    }

    public String getTravelTo() {
        return TravelTo;
    }

    public void setTravelTo(String TravelTo) {
        this.TravelTo = TravelTo;
    }
    
}
